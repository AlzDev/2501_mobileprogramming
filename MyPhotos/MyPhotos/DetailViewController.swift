//
//  DetailViewController.swift
//  MyPhotos
//
//  Created by Alan on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//
import UIKit

class DetailViewController: UIViewController {
    // MARK: - Variables
    var detailItem: photo! = photo()
    var delegate: PhotoProtocol?
    var photoIndex: Int = 0
    
    
    
    @IBAction func deleteConfirmation(sender: AnyObject) {
        let alert = UIAlertController(title: "Confirm Delete", message: "Do you really wish to delete \(detailItem.title)?", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { alertAction in
        // Delete item here
        self.delegate?.deleteConfirmation(self.detailItem)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { alertAction in })
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
	

    
    //MARK: Backwards navigation - saving text in fields
    //Assign any changes made to the fields back to the relevant sections in the array
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
			
		// If the url field is not empty
        if (txtUrl.text != "") {
			// check if this is a new item by testing if a url exists already
			if (detailItem.url == "") {
				detailItem.title = txtTitle.text!
				detailItem.tags = txtTags.text!.componentsSeparatedByString(", ")
				detailItem.url = txtUrl.text!
			
				// Add new photo to the grid array
				delegate?.newPhoto(detailItem)
			} else {
				// Update current photo details
				detailItem.title = txtTitle.text!
				detailItem.tags = txtTags.text!.componentsSeparatedByString(", ")
				detailItem.url = txtUrl.text!
			}
		}
    }
	
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: Text Field Assigns
        //Display the information in the photo specific to the array chosen (or a new photo completely) in the proper fields
        //Do any additional setup after loading the view.
        txtTitle?.text = detailItem.title
        txtTags?.text = detailItem.tags!.joinWithSeparator(", ")
        txtUrl?.text = detailItem.url

		downloadImage(txtUrl.text!)
    }
	
	
	///This function handles the reload of an image in the details screen
	///
	///
	///
	///Usage:
	///
	///		Reloads the Image upon entering a new URL
	///
	/// -parameters:
	/// - sender: textField
	///
	/// -returns: Bool
	func textFieldShouldReturn(texField: UITextField!) -> Bool {
		downloadImage(txtUrl.text!)
		
		texField.resignFirstResponder()
		return true
	}
	
	///This function handles the downloading of an image
	///
	///
	///
	///Usage:
	///
	///		Downloads the image using the url provided
	///
	/// -parameters:
	/// - sender: URL (String)
	///
	/// -returns: Nothing
	func downloadImage(URL: String) {
		//Grabs the URl in the URl.text field, and turns it into a displayable image through NSData and UIImage and NSURL
		if let url = NSURL(string: URL) {
			let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)
			
			dispatch_async(queue) {
				if let data = NSData(contentsOfURL: url), let image = UIImage(data: data) {
					let mainQueue = dispatch_get_main_queue()
					dispatch_async(mainQueue, {
						// set cell thumbnail image
						self.imageView.image = image
					})
				} else {
					print("Could not download image: '\(URL)'")
				}
			}
		}
	}
	
	
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


	
    // MARK: - Outlets
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtTags: UITextField!
    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    

    
}