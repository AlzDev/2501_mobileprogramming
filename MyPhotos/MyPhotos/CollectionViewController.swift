//
//  ViewController.swift
//  MyPhotos
//
//  Created by Alan on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

/**
Controls the Collection View in the storyboard
 
 - Author
 Alan Schiffler
 
 - variables:
 - detailViewController: passes the DetailViewController
 - Array: hold the photoGrid array in a variable
 - Important:
 This class has to be initialized before use
 -Verson
 1.0
 */
class CollectionViewController: UICollectionViewController, PhotoProtocol {
    
    //MARK: Collection Variables
    var grid: photoGrid = photoGrid()
	var index: Int = 0
    
    //
    
    func deleteConfirmation(deletedPhoto: photo) {
        grid.entries.removeAtIndex(index)
        navigationController?.popViewControllerAnimated(true)
        self.collectionView!.reloadData()
    }

    
	///This function handles the addition of a new photo upon hitting the add button
	///
	///
	///
	///Usage:
	///
	///		Adds a new photo to the array
	///
	/// -parameters:
	/// - sender: photo
	///
	/// -returns: Nothing
    func newPhoto(NewPhoto: photo) {
        grid.entries.append(NewPhoto)
        self.collectionView!.reloadData()
    }
	
	///When the use swipes their finger right, it means that the view is getting swiped left
	///grabs the image that was prior to the currently displayed one in the array, and
	///redisplays
	///
	///Usage:
	///
	///		When the user swipes their finger right, move the screen left.
	///
	/// -parameters:
	/// - sender: AnyObject
	///
	/// -returns: Nothing
	func swipeLeft(VC: ViewImageController) {
		//prev photo
		index -= 1
		if index < 0 {
			index = (grid.entries.count - 1)
		}
		VC.detailItem = grid.entries[index]
	}
	
	///When the use swipes their finger left, it means that the view is getting swiped right
	///grabs the image that is after the currently displayed one in the array, and
	///redisplays
	///
	///Usage:
	///
	///		When the user swipes their finger left, move the screen right.
	///
	/// -parameters:
	/// - sender: AnyObject
	///
	/// -returns: Nothing
	func swipeRight(VC: ViewImageController) {
		//next photo
		index += 1
		if index > (grid.entries.count - 1) {
			index = 0
		}
		VC.detailItem = grid.entries[index]
	}
    
	
	// Give the segues direction and functionality
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		//let controller: DetailViewController = segue.destinationViewController as! DetailViewController
		//controller.delegate = self
		
		//If you take the segue to the detail view of the app, do the following
		if segue.identifier == "showPhoto" {
			let ViewVC: ViewImageController = segue.destinationViewController as! ViewImageController
			ViewVC.delegate = self
			
			
			if let indexPath = self.collectionView?.indexPathForCell(sender as! UICollectionViewCell) {
				ViewVC.detailItem = grid.entries[indexPath.row]
				index = indexPath.row
			}
			
		} else if segue.identifier == "showNew" {
			let NewVC: DetailViewController = segue.destinationViewController as! DetailViewController
			NewVC.delegate = self
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // This alters the battery bar top area to be white
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        // Testing data
        let entry = photo(title: "Cat1", tags:["dog", "cat", "Mouse"], url: "https://i.ytimg.com/vi/tntOCGkgt98/maxresdefault.jpg")
		let entry1 = photo(title: "Cat2", tags:["meow", "cat", "Mouse"], url: "https://i.ytimg.com/vi/sTX8qbOtPN8/hqdefault.jpg")
		let entry2 = photo(title: "Dog", tags:["Woof", "Bone", "Bark"], url: "https://i.ytimg.com/vi/MveqXxB12YA/hqdefault.jpg")
        //This adds the entry photo into the entries array from the photoGrid
        grid.entries.append(entry)
		grid.entries.append(entry1)
		grid.entries.append(entry2)

    }
	
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // makes the view have asmany cells are records in the array
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return grid.entries.count
    }
    
    
    
    // Willappear function
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView!.reloadData()
        grid.savePropertyList()
    }
    
    
    
    //MARK: Grid and Cell Functionality
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! GridItemCollectionViewCell

        
        //assign the correct entry's URL to the thumbnail and display in a grid view
        let urlName = grid.entries[indexPath.row].url
        if let url = NSURL(string: urlName) {
            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)
            
            dispatch_async(queue) {
                if let data = NSData(contentsOfURL: url), let image = UIImage(data: data) {
                    let mainQueue = dispatch_get_main_queue()
                    dispatch_async(mainQueue, {
                        // set cell thumbnail image
                        cell.cellPhoto.image = image
                    })
                } else {
                    //Error handle if the URL can not be downloaded
                    print("Could not download image: '\(urlName)'")
                }
            }
        }
        
        return cell
    }
	@IBOutlet var tapDeteced: UITapGestureRecognizer!
}





