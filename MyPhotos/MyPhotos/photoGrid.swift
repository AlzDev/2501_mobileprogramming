//
//  PhotoCollection.swift
//  MyPhotos
//
//  Created by Alan on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

/**
 Holds the photoGrid entries array (for the photos)
 
 - Author
 Alan Schiffler
 
 - variables:
 -entries: the array that holds all the photo variables
 
 - Important:
 This class has to be initialized before use
 -Verson
 1.0
 */

class photoGrid {

    
    //MARK: photoGrid class Variables
    //(Grabs the Photo details and adds this to an Array)
    var entries = [photo]();
    
    init() {
        // IF property list exists {
//        self.entries = propertyList.map { (param: AnyObject) -> photo in
//            let pl = param as! NSDictionary
//            let savedPhoto = photo(toDict: pl)
//            return savedPhoto!
//        }
//     } ELSE {
        self.entries = [photo]()
//     }
    }
    
    // Load data from Property List
    func loadPropertyList(propertyList: NSArray) {
        self.entries = propertyList.map { (param: AnyObject) -> photo in
            let pl = param as! NSDictionary
            let savedPhoto = photo(toDict: pl)
            return savedPhoto!
        }
    }
    
    func savePropertyList() {
        print("Saved data")
        let propertyList: NSArray = self.entries.map { $0.toDict() }
        
        let docDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let filePath = docDirectory.stringByAppendingPathComponent("photos.plist")
        
        propertyList.writeToFile(filePath, atomically: true)
    }
}
