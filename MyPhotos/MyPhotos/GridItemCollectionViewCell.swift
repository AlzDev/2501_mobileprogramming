//
//  GridItemCollectionViewCell.swift
//  MyPhotos
//
//  Created by Alan on 22/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

/**
 Holds the cellPhoto Grid Handler
 
 - Author
 Alan Schiffler
 
 - Important:
 This is needed for the CollectionView to display the image in the correct cell
 
 -Verson
 1.0
 */
class GridItemCollectionViewCell: UICollectionViewCell {
    // MARK: Photo Cell Grid Handler
    // This is used by the CollectionView to generate the correct photos in the correct cell
    @IBOutlet weak var cellPhoto: UIImageView!
}
