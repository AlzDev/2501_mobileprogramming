/*:
### Tables Of Contents

1. [The Photo Class] 
2. [The photoGrid Class]
3. [The Grid Item Controller]
4. [The Collection View]
5. [The Detail View]

*/

 
//
//  Photo.swift
//  MyPhotos
//
//  Created by Alan Schiffler - s5006515 on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

/**
 Holds the variables for the photo class
 
 - Author
 Alan Schiffler
 
 - variables:
 -title: the title string for the photo
 -tags: the tag string array for the photo
 -url: the url string for the photo
 - Important:
 This class is used in the photoGrid class Array.
 -Verson
 1.0
 */
class photo {

    //MARK: Photo Variables Declared
    var title : String
    var tags : [String]?
    var url : String
    

    
    //MARK: Photo Initializer
    init() {
        self.title = ""
        self.tags = []
        self.url = ""
    }
    
    init(title: String, tags: [String]? = nil, url: String) {
        self.title = title;
        self.tags = tags;
        self.url = url;
    }
    
    convenience init?(toDict: NSDictionary) {
        self.init()
        
        // Title
        guard let title = toDict["title"] as? String else {
            return nil
        }
        self.title = title
        
        // Tags
        guard let tags = toDict["tags"] as? [String] else {
            return nil
        }
        self.tags = tags
        
        // URL
        guard let url = toDict["url"] as? String else {
            return nil
        }
        self.url = url
    }

    func toDict() -> NSDictionary {
        return ["name": self.title, "tags": (self.tags)!, "url": self.url]
    }
    
    

}

