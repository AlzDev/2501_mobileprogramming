//
//  PhotoDelegate.swift
//  MyPhotos
//
//  Created by Alan on 11/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

protocol PhotoProtocol {
	func deleteConfirmation(deletedPhoto: photo)
	func newPhoto(NewPhoto: photo)
	
	func swipeLeft(VC: ViewImageController)
	func swipeRight(VC: ViewImageController)
}