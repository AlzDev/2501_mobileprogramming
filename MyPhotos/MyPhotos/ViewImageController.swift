//
//  FullImageView.swift
//  MyPhotos
//
//  Created by Alan on 8/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//
import UIKit
import Foundation

class ViewImageController: UIViewController {
	// MARK: - Variables
	var detailItem: photo = photo()
	var delegate: PhotoProtocol?
	
	override func viewDidLoad() {
		super.viewDidLoad()

		//MARK: URLIMG for DetailView
		//Grabs the URl in the URl.text field, and turns it into a displayable image through NSData and UIImage and NSURL
		downloadImage(detailItem.url)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.navigationBar.hidden = true
		UIApplication.sharedApplication().statusBarHidden = true
	}
	

	
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		
		if segue.identifier == "showDetails" {
			let DetailVC: DetailViewController = segue.destinationViewController as! DetailViewController
			DetailVC.delegate = self.delegate
			DetailVC.detailItem = self.detailItem
		}
		
	}
	
	
	@IBAction func swipeRight(sender: AnyObject) {
		//next photo
		delegate?.swipeRight(self)
		
		//Redownload Image
		//MARK: URLIMG for DetailView
		//Grabs the URl in the URl.text field, and turns it into a displayable image through NSData and UIImage and NSURL
		downloadImage(detailItem.url)

		
	}
	
	@IBAction func swipeLeft(sender: AnyObject) {
		// previous photo
		
		delegate?.swipeLeft(self)
		//Redownload Photo
		//MARK: URLIMG for DetailView
		//Grabs the URl in the URl.text field, and turns it into a displayable image through NSData and UIImage and NSURL
		downloadImage(detailItem.url)

	}
	
	///This function handles the downloading of an image
	///
	///
	///
	///Usage:
	///
	///		Downloads the image using the url provided
	///
	/// -parameters:
	/// - sender: URL (String)
	///
	/// -returns: Nothing
	func downloadImage(URL: String) {
		//Grabs the URl in the URl.text field, and turns it into a displayable image through NSData and UIImage and NSURL
		if let url = NSURL(string: URL) {
			let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)
			
			dispatch_async(queue) {
				if let data = NSData(contentsOfURL: url), let image = UIImage(data: data) {
					let mainQueue = dispatch_get_main_queue()
					dispatch_async(mainQueue, {
						// set cell thumbnail image
						self.imageView.image = image
					})
				} else {
					print("Could not download image: '\(URL)'")
				}
			}
		}
	}

	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		self.navigationController?.navigationBar.hidden = false
		UIApplication.sharedApplication().statusBarHidden = false
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	
	
	// MARK: - Outlets
	
	@IBOutlet weak var imageView: UIImageView!

	
	
	
	
	
}