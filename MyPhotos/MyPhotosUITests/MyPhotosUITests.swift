//
//  MyPhotosUITests.swift
//  MyPhotosUITests
//
//  Created by Alan on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import XCTest

class MyPhotosUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        // Set tp true for testing purposes
        continueAfterFailure = true
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddButton() {
        
        let app = XCUIApplication()
        
        
        
        let addButton =  app.navigationBars["Photos"].buttons["Add"]
        addButton.tap()
    }
    
    //Wait for x amount of seconds
    func wait(delay: NSTimeInterval = 2) {
        let runloop = NSRunLoop.mainRunLoop()
        let someTimeInTheFuture = NSDate(timeIntervalSinceNow: delay)
        runloop.runUntilDate(someTimeInTheFuture)
    }
    
    func numberOfTries(value: XCUIElement, _ t: NSTimeInterval = 3) {
        let exists = NSPredicate(format: "exists == true")
        let expectation = expectationForPredicate(exists, evaluatedWithObject: value, handler: nil)
        waitForExpectationsWithTimeout(t, handler: nil)
        print(expectation)
        
    }
    
    func testDetailBackButton() {
        let app = XCUIApplication()
        let addButton = app.navigationBars["Photos"].buttons["Add"]
        let backButton = app.navigationBars["UIView"].buttons["Photos"]
        
        while !addButton.exists {
        wait() //waits 2 seconds
        }
        addButton.tap()
        
        wait(1) //wait 1 sec
        
        while !backButton.exists {
        wait() //wait 2 seconds
        }
        backButton.tap()
    }
	
	func testAddPhoto(){
		
		
		let app = XCUIApplication()
		app.navigationBars["Photos"].buttons["Add"].tap()
		
		let element = app.otherElements.containingType(.NavigationBar, identifier:"Photo View").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
		let textField = element.childrenMatchingType(.TextField).elementBoundByIndex(0)
		textField.tap()
		textField.typeText("Test")
		
		let textField2 = element.childrenMatchingType(.TextField).elementBoundByIndex(1)
		textField2.tap()
		textField2.typeText("Test")
		
		let textField3 = element.childrenMatchingType(.TextField).elementBoundByIndex(2)
		textField3.tap()
		textField3.typeText("Test")
		app.navigationBars["Photo View"].buttons["Photos"].tap()
		
	}
	
	func testDeleteButton() {
		
		
		let app = XCUIApplication()
		app.navigationBars["Photos"].buttons["Add"].tap()
		
		let element = app.otherElements.containingType(.NavigationBar, identifier:"Photo View").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
		let textField = element.childrenMatchingType(.TextField).elementBoundByIndex(0)
		textField.tap()
		textField.tap()
		textField.typeText("cat")
		
		let textField2 = element.childrenMatchingType(.TextField).elementBoundByIndex(1)
		textField2.tap()
		textField2.tap()
		textField2.typeText("cat")
		
		let textField3 = element.childrenMatchingType(.TextField).elementBoundByIndex(2)
		textField3.tap()
		textField3.tap()
		textField3.typeText("cat")
		
		let photoViewNavigationBar = app.navigationBars["Photo View"]
		photoViewNavigationBar.buttons["Photos"].tap()
		app.collectionViews.cells.otherElements.childrenMatchingType(.Image).element.tap()
		photoViewNavigationBar.buttons["Delete"].tap()
		app.sheets["Confirm Delete"].collectionViews.buttons["Delete"].tap()
		
		
	}
	
	func testDeleteCancel() {
		
		
		let app = XCUIApplication()
		app.navigationBars["Photos"].buttons["Add"].tap()
		
		let element = app.otherElements.containingType(.NavigationBar, identifier:"Photo View").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
		let textField = element.childrenMatchingType(.TextField).elementBoundByIndex(0)
		textField.tap()
		textField.typeText("cat2")
		
		let textField2 = element.childrenMatchingType(.TextField).elementBoundByIndex(1)
		textField2.tap()
		textField2.typeText("cat")
		
		let textField3 = element.childrenMatchingType(.TextField).elementBoundByIndex(2)
		textField3.tap()
		textField3.typeText("cat2")
		
		let photoViewNavigationBar = app.navigationBars["Photo View"]
		let photosButton = photoViewNavigationBar.buttons["Photos"]
		photosButton.tap()
		app.collectionViews.cells.otherElements.childrenMatchingType(.Image).element.tap()
		photoViewNavigationBar.buttons["Delete"].tap()
		app.sheets["Confirm Delete"].buttons["Cancel"].tap()
		photosButton.tap()
		
		
	}
}
