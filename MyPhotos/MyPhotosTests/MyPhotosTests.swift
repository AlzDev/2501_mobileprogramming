//
//  MyPhotosTests.swift
//  MyPhotosTests
//
//  Created by Alan on 20/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit
import XCTest
@testable import MyPhotos

class MyPhotosTests: XCTestCase {
    let someTitle = "I AM A TITLE!"
    let someTag = "Australian Photo, QLD, Brisbane"
    let someURL = "https://www.google.com.au/search?q=photo&espv=2&biw=1494&bih=863&tbm=isch&imgil=n_rB1UpJAtsXvM%253A%253Byr5EgF7HVsWHPM%253Bhttps%25253A%25252F%25252Fwww.pexels.com%25252Fphoto%25252Ffield-sky-nature-agriculture-7976%25252F&source=iu&pf=m&fir=n_rB1UpJAtsXvM%253A%252Cyr5EgF7HVsWHPM%252C_&usg=__NSAC9z4axdMdTah3-3g2D1gd06o%3D&ved=0ahUKEwiTmuPIw5zMAhVEspQKHX8IBcEQyjcIOw&ei=hBkXV5PICcTk0gT_kJSIDA#imgrc=n_rB1UpJAtsXvM%3A"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //
    // Test the photo array as is when empty
    //
    
    func testEmptyPhotoArray() {
        let pGrid = photoGrid()
        XCTAssertEqual(pGrid.entries.count, 0)
    }
    
    //
    // This test handles adding a single record to the array
    // 
    
    func testAddToPhotoArray() {
        let pGrid = photoGrid()
        
        let entry = photo(title: "Photo1", tags:["dog", "cat", "poo"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        
        pGrid.entries.append(entry)
        
        XCTAssertEqual(pGrid.entries.count, 1)
        XCTAssertTrue(pGrid.entries[0] === entry)
        
    }
    
    //
    // This test handles adding multiple records to the array
    //
    
    func testMultipleAddPhotoArray(){
        
        let pGrid = photoGrid()
        let entry1 = photo(title: "Photo1", tags: ["This is the first photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        pGrid.entries.append(entry1)
        XCTAssertEqual(pGrid.entries.count, 1)
        XCTAssertTrue(pGrid.entries[0] === entry1)
    
        let entry2 = photo(title: "Photo2", tags:["This is the second photo being tested"], url: "http://www.signaturehardware.com/media/catalog/product/cache/1/image/1500x/9df78eab33525d08d6e5fb8d27136e95/1/0/10490_2.jpg")
        pGrid.entries.insert(entry2, atIndex: 0)
        XCTAssertEqual(pGrid.entries.count, 2)
        XCTAssertTrue(pGrid.entries[1] === entry1)
        XCTAssertTrue(pGrid.entries[0] === entry2)
        
        let entry3 = photo(title: "Photo3", tags: ["Number 3!"], url: "http://www.lombard.com.au/www/746/files/35173.jpg")
        pGrid.entries.insert(entry3, atIndex: 1)
        XCTAssertEqual(pGrid.entries.count, 3)
        XCTAssertTrue(pGrid.entries[2] === entry1)
        XCTAssertTrue(pGrid.entries[0] === entry2)
        XCTAssertTrue(pGrid.entries[1] === entry3)
        
    }
    
    ///
    /// This test handles an item being added at the start of an array
    ///
    
    func testAddStart(){
    
        let pGrid = photoGrid()
        let entry1 = photo(title: "Photo1", tags:["This is the first photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        pGrid.entries.append(entry1)
    
        let newEntry = photo(title: "AddedPhoto", tags:["This is the initial photo being tested"], url: "www.google.com")
        pGrid.entries.insert(newEntry, atIndex: 0)
    
        XCTAssertEqual(pGrid.entries.count, 2)
        XCTAssertTrue(pGrid.entries[0] === newEntry)
    }
    
    ///
    /// This test handles adding to the middle of the array
    ///
    
    func testAddMiddle(){

        let pGrid = photoGrid()
        let entry1 = photo(title: "Photo1", tags:["This is the first photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        pGrid.entries.append(entry1)
        let entry2 = photo(title: "Photo2", tags:["This is the 2nd photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        pGrid.entries.append(entry2)


        let newEntry = photo(title: "AddedPhoto", tags:["This is the initial photo being tested"], url: "www.google.com")
        pGrid.entries.insert(newEntry, atIndex: 1)

        XCTAssertEqual(pGrid.entries.count, 3)
        XCTAssertTrue(pGrid.entries[1] === newEntry)
}
    
    ///
    /// This test handles adding to the end of the array
    ///
    
    func testAddEnd(){
        
        let pGrid = photoGrid()
        let entry1 = photo(title: "Photo1", tags:["This is the first photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
            pGrid.entries.append(entry1)
        let entry2 = photo(title: "Photo2", tags:["This is the 2nd photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
            pGrid.entries.append(entry2)
        
        
        let newEntry = photo(title: "AddedPhoto", tags:["This is the initial photo being tested"], url: "www.google.com")
        pGrid.entries.insert(newEntry, atIndex: 2)
        
        XCTAssertEqual(pGrid.entries.count, 3)
        XCTAssertTrue(pGrid.entries[2] === newEntry)
    }

    ///
    /// This test handles adding multiple records to the array, and then deleting them
    ///
    
    func testMultipleDeletePhotoArray(){
        
        let pGrid = photoGrid()
        let entry1 = photo(title: "Photo1", tags:["This is the first photo being tested"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439")
        pGrid.entries.append(entry1)
        XCTAssertEqual(pGrid.entries.count, 1)
        XCTAssertTrue(pGrid.entries[0] === entry1)
        
        let entry2 = photo(title: "Photo2", tags:["This is the second photo being tested"], url: "http://www.signaturehardware.com/media/catalog/product/cache/1/image/1500x/9df78eab33525d08d6e5fb8d27136e95/1/0/10490_2.jpg")
        pGrid.entries.insert(entry2, atIndex: 0)
        XCTAssertEqual(pGrid.entries.count, 2)
        XCTAssertTrue(pGrid.entries[1] === entry1)
        XCTAssertTrue(pGrid.entries[0] === entry2)
        
        let entry3 = photo(title: "Photo3", tags:["Number 3!"], url: "http://www.lombard.com.au/www/746/files/35173.jpg")
        pGrid.entries.insert(entry3, atIndex: 1)
        XCTAssertEqual(pGrid.entries.count, 3)
        XCTAssertTrue(pGrid.entries[2] === entry1)
        XCTAssertTrue(pGrid.entries[0] === entry2)
        XCTAssertTrue(pGrid.entries[1] === entry3)

        let removedEntry2 = pGrid.entries.removeAtIndex(0)
        XCTAssertTrue(removedEntry2 === entry2)
        XCTAssertEqual(pGrid.entries.count, 2)
        XCTAssertTrue(pGrid.entries[1] === entry1)
        XCTAssertTrue(pGrid.entries[0] === entry3)
        
        let removedEntry1 = pGrid.entries.removeAtIndex(1)
        XCTAssertTrue(removedEntry1 === entry1)
        XCTAssertEqual(pGrid.entries.count, 1)
        XCTAssertTrue(pGrid.entries[0] === entry3)

        
        let removedEntry3 = pGrid.entries.removeAtIndex(0)
        XCTAssertTrue(removedEntry3 === entry3)
        XCTAssertEqual(pGrid.entries.count, 0)
  
    }
    
    func testSave() {
        let photos: [photo] = [photo(title: "first title", tags: ["1st tag"], url: "http://vignette4.wikia.nocookie.net/lyricwiki/images/9/98/Number_one.png/revision/latest?cb=20080802041439"), photo(title: "first title", tags: ["2nd tag"], url: "http://www.lombard.com.au/www/746/files/35173.jpg")]
        
        let docDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let filePath = docDirectory.stringByAppendingPathComponent("photos.plost")
        
        let storePhoto: NSArray = photos.map { $0.toDict()}
        
        storePhoto
        storePhoto.writeToFile(filePath, atomically: true)
        
    }

}
