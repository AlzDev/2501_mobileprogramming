//
//  MasterViewController.swift
//  ContactList
//
//  Created by Alan on 7/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    // Declare Variables
    var detailViewController: DetailViewController? = nil
    var listedContacts: ContactList = ContactList()

    
    // Testing Data
    let contact1: ContactListEntry = ContactListEntry(firstName: "Alan", lastName: "Schiffler", yearOfBirth: 1990, middleName: "Lloyd", address: "1/2 Monkey street", phoneNumber: "0430411499")
    let contact2: ContactListEntry = ContactListEntry(firstName: "Ella", lastName: "Moser", yearOfBirth: 1994, middleName: "Yvonne", address: "1/2 Cat street", phoneNumber: "55112221")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        listedContacts.entries.append(contact1)
        listedContacts.entries.append(contact2)
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        // creating the instance of the array
        //let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        //self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
        
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                //Handles contactListEntry Object
                let object = listedContacts.entries[indexPath.row]
                controller.detailItem = object
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        } else if segue.identifier == "showNew" {
            
            //Creates a new contactList Objectect
            let createNewObject = ContactListEntry(firstName: "", lastName: "", yearOfBirth: 0, middleName: "", address: "", phoneNumber: "")
            
            listedContacts.entries.append(createNewObject)
            controller.detailItem = createNewObject

            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        //        let object = objects[indexPath.row] as! NSDate
        let object = listedContacts.entries [indexPath.row]
        
        cell.textLabel!.text = object.fullName()
        //            .description
        //
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listedContacts.entries.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            listedContacts.entries.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    
    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


