//
//  ContactListEntry.swift
//  ContactList
//
//  Created by Alan on 8/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

class ContactListEntry: Person {
    var address:String?
    var phoneNumber:String?
    
    init(  firstName: String, lastName: String,  yearOfBirth: Int? = nil, middleName: String? = nil, address: String? = nil, phoneNumber: String? = nil) {
        self.address = address
        self.phoneNumber = phoneNumber
        super.init(firstName: firstName, lastName: lastName, yearOfBirth: yearOfBirth, middleName: middleName)
        
        
    }
    
}