//
//  DetailViewController.swift
//  ContactList
//
//  Created by Alan on 7/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var listedContacts: ContactList = ContactList()
    var detailItem: ContactListEntry! = ContactListEntry(firstName: "", lastName: "", yearOfBirth: 0000, middleName: "", address: "", phoneNumber: "") {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        updateContact()
    }

    func configureView() {
        
        // Update the user interface for the detail item.
        txtFirstName?.text = detailItem.firstName
        txtMiddleName?.text = detailItem.middleName
        txtLastName?.text = detailItem.lastName
        txtYOB?.text = "\(detailItem.yearOfBirth)"
        txtPhone?.text = detailItem.phoneNumber
        txtAddress?.text = detailItem.address
    }
    
    // Save contact details
    func updateContact() {
        detailItem.firstName = txtFirstName.text!
        detailItem.middleName = txtMiddleName.text!
        detailItem.lastName = txtLastName.text!
        detailItem.phoneNumber = txtPhone.text!
        detailItem.yearOfBirth = Int(txtYOB.text!)
        detailItem.address = txtAddress.text!
    }
    

    // Cancel button
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (txtFirstName?.text == "" || txtLastName?.text == "") {
            // Do nothing
        } else {
            txtFirstName.text = detailItem.firstName
            txtMiddleName.text = detailItem.middleName
            txtLastName.text = detailItem.lastName
            txtPhone.text = detailItem.phoneNumber
            txtAddress.text = detailItem.address
            txtYOB.text = String(detailItem.yearOfBirth)
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Text Fields
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtYOB: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
}

