//
//  ContactList.swift
//  ContactList
//
//  Created by Alan on 8/04/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

public class ContactList {
    
    var entries = [ContactListEntry]();
    
    init() {
        entries = [ContactListEntry]();
    }
}