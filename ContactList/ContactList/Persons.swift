//
//  Persons.swift
//  Personal
//
//  Created by Alan on 11/03/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import Foundation

class Person  {                                  									// Class is as per usual
                                                                                    // Strings had to be assigned Nil due to consistant error
    var firstName:String                                                            // Declare variable and THEN the type
    var lastName:String
    var middleName: String?                                                         // Optional variable to be followed with ?
    var yearOfBirth:Int!
    var age:Int! {                                                                  // Int variable that is empty
        get {
            guard let _ =  yearOfBirth else {
                return nil
            }
        
            return getCurrentYear() - yearOfBirth!
        }
        set {
            yearOfBirth = getCurrentYear() - newValue;
        }
    }
    
    
    // This is the convenience init. Age is Int! because it may be empty, avoids error catch
    // Strings had to be assigned Nil due to consistant error
    
    convenience init (firstName: String, lastName: String, age: Int!, middleName: String? = nil ) {
        self.init(firstName: firstName, lastName: lastName, yearOfBirth: 333, middleName: middleName)
        self.age = age
    }
    
    // Initialise everything for Person
    // Strings had to be assigned Nil due to consistant error
    
    init(firstName: String, lastName: String, yearOfBirth: Int? = nil, middleName: String? = nil) {
        self.firstName = firstName;
        self.middleName = middleName;
        self.lastName = lastName;
        self.yearOfBirth = yearOfBirth;
        
        }
    

    // The below function returns the current year that will later subtract from the year the user has entered, thus producting the age
    
    func getCurrentYear() -> Int{
        let todaysDate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let currentYear = calendar.component(.Year, fromDate: todaysDate)
        
        return currentYear;
    }
    
    // If there is a middlename, do the top, else omit middle name completely and just display first and last name
    func fullName() -> String {
        var name : String = firstName + " ";
            if (middleName != nil) {
                name += "" +  middleName! + " " + lastName;
            } else {
                name += lastName;
            }
                return name;
    }
    
    func printDetails() -> String {
        return fullName() + ", " + String(age);
    }

}
