//
//  ViewController.swift
//  WebViewer
//
//  Created by Alan on 11/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var txtSearch: UISearchBar!
	@IBOutlet weak var webView: UIWebView!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		txtSearch.autocapitalizationType = UITextAutocapitalizationType.None
	}

	///Ensures the search bar has text before enabling it to load the url
	///
	///
	/// Usage:
	///	Checks if the search bar is empty, if not, load URL, else return nothing
	///
	/// -parameters
	/// - searchbar (url)
	///
	/// -returns: Nothing
	func searchBarSearchButtonClicked(searchBar: UISearchBar) {
		guard let url = NSURL(string: txtSearch.text!)
			else {
				return
		}
		let request = NSURLRequest(URL: url)
		txtSearch.endEditing(true)
		webView.loadRequest(request)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}



}

