//
//  WebViewerUITests.swift
//  WebViewerUITests
//
//  Created by Alan on 11/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import XCTest

class WebViewerUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
}
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSearch() {
		
		let app = XCUIApplication()
		let enterUrlHereSearchField = app.searchFields["Enter URL Here"]
		enterUrlHereSearchField.tap()
		enterUrlHereSearchField.typeText("https://www.google.com")
		app.typeText("\r")
		
		sleep(5)
		
		XCTAssert(app.staticTexts["Sign in"].exists)
    }
    
}
